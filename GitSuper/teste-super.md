# Projeto GitSuper

Aplicativo para processo seletivo da empresa 'Super Revendedores' https://bitbucket.org/adminsuper/desafio-android-super-revendedores/src/master/
Esse arquivo visa explicar a implementação do código de forma simples e objetiva.

# Da Implementação
Para esse projeto foi utilizado o padrão MVVM junstamente aos Componentes de Arquitetura Android (https://developer.android.com/topic/libraries/architecture/) disponibilizado pela Google para um melhor aproveitamento das capacidades do Android.

# Das Libs Externas
Visando deixar o aplicativo o mais compatível e testável possível o número de libs externas foi reduzido ao máximo. As libs usadas s~ão descritas abaixo:
  - Glide - Para o Carregamento de Imagens (Remotas e/ou em Cache)
  - Retrofit e OkHttp - Para comunicação efetiva com a API utilizada.
  - Gson - Para tratamento das respostas json da API com o mínimo de código possível.
  - Crashlytics - Para acompanhamento de possíveis falhas do aplicativo para que se tenha um melhor feedback e agilidade na correção do problema. 

Todo o restante dos componentes e libs utilizadas são nativos do próprio sistema para que a compatibilidade seja sempre a melhor possível. 
  
# Dos Testes
Os testes unitários empregados foram baseados na espresso (https://developer.android.com/training/testing/espresso/). No momento apenas estruturei uns poucos testes (alguns vazios) apenas para ciência de que precisarão ser feitos mas acredito que o app em si já consiga demonstrar o conhecimento atual para esse teste.

# Considerações finais
Fiz todo o possível para que o aplicativo ficasse o mais organizado, leve e testável possível. Tenho ci^ência que alguns pontos podem ser melhorados mas para a finalização desse teste acredito que o app possa atender as necessidades atuais.