package project.android.gitsuper._api.repository;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.arch.paging.PagedList.Config.Builder;
import android.support.annotation.MainThread;

import project.android.gitsuper.github.home.model.DataLoadState;
import project.android.gitsuper.github.home.model.Item;
import project.android.gitsuper._api.data.ItemDataFactory;

import static android.arch.lifecycle.Transformations.switchMap;


public class ItemRepositoryImpl implements ItemRepository {

    private ItemDataFactory dataSourceFactory;
    private static final int PAGE_SIZE = 30;

    public ItemRepositoryImpl() {
        dataSourceFactory = new ItemDataFactory();
    }

    @Override
    @MainThread
    public LiveData<PagedList<Item>> getRepositories() {
        PagedList.Config config = new Builder().setPageSize(PAGE_SIZE).build();
        return (LiveData<PagedList<Item>>) new LivePagedListBuilder(dataSourceFactory, config).setInitialLoadKey(1).build();
    }

    public LiveData<DataLoadState> getDataLoadStatus() {
        return switchMap(dataSourceFactory.datasourceLiveData, dataSource -> dataSource.loadState);
    }

}