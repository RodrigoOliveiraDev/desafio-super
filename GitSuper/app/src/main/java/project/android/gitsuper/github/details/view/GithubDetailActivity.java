package project.android.gitsuper.github.details.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import project.android.gitsuper.R;
import project.android.gitsuper.databinding.ActivityDetailGithubBinding;
import project.android.gitsuper.github._util.MyDividerItemDecoration;
import project.android.gitsuper.github.details.viewmodel.GithubDetailViewModel;
import project.android.gitsuper.github.home.model.Item;

public class GithubDetailActivity extends AppCompatActivity implements PullRequestsAdapter.OnActionListerner {

    private static final String TAG = GithubDetailActivity.class.getSimpleName();

    private GithubDetailViewModel viewModel;
    private PullRequestsAdapter pullAdapter;
    private ActivityDetailGithubBinding binding;
    private Item itemSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_github);

        configToolbar();
        configRecycleView();

        Intent b = getIntent();
        if (b != null) {
            itemSelected = (Item) b.getSerializableExtra("item_selected");
            binding.headerDetail.setPost(itemSelected);
            configViewModel(itemSelected.owner.login, itemSelected.name);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openPullRequestUrl(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    private void configToolbar() {
        setSupportActionBar(binding.toolbarMain.toolbar);
        setTitle(R.string.s_detail_git);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    private void configRecycleView() {
        pullAdapter = new PullRequestsAdapter(new ArrayList<>(), this);
        binding.rvMain.rvList.setLayoutManager(new LinearLayoutManager(this));
        binding.rvMain.rvList.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 0));
        binding.rvMain.rvList.setAdapter(pullAdapter);
    }

    private void configViewModel(String create, String repository) {
        viewModel = ViewModelProviders.of(this).get(GithubDetailViewModel.class);

        viewModel.getPullRequests(create, repository).observe(GithubDetailActivity.this, pullRequestModels -> {
            if (pullRequestModels != null && pullRequestModels.size() > 0) {
                pullAdapter.addItems(pullRequestModels);
                showRvImport();
            } else {
                showEmptyList();
            }
        });
    }

    private void showRvImport() {
        binding.loadMain.pbLoad.setVisibility(View.GONE);
        binding.tvMain.llEmptyValues.setVisibility(View.GONE);
        binding.rvMain.rvList.setVisibility(View.VISIBLE);
    }

    private void showEmptyList() {
        binding.loadMain.pbLoad.setVisibility(View.GONE);
        binding.rvMain.rvList.setVisibility(View.GONE);
        binding.tvMain.tvNoValues.setText(getString(R.string.s_without_forks));
        binding.tvMain.llEmptyValues.setVisibility(View.VISIBLE);
    }

    private void showFailed(){

    }

}