package project.android.gitsuper.github.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rodrigo Oliveira on 26/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
public class License implements Serializable {

    @SerializedName("key")
    @Expose
    public String key;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("spdx_id")
    @Expose
    public String spdxId;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("node_id")
    @Expose
    public String nodeId;

    public License() {
    }

}