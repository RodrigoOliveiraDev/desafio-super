package project.android.gitsuper.github.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rodrigo Oliveira on 26/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
public class RepositoryModel implements Serializable {

    @SerializedName("total_count")
    @Expose
    public Integer totalCount;
    @SerializedName("incomplete_results")
    @Expose
    public Boolean incompleteResults;
    @SerializedName("items")
    @Expose
    public List<Item> items = null;

    public RepositoryModel() {
    }

}