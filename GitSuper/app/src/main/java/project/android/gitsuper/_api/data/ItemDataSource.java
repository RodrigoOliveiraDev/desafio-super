package project.android.gitsuper._api.data;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import java.io.IOException;

import project.android.gitsuper._api.ApiInterface;
import project.android.gitsuper.github.home.model.DataLoadState;
import project.android.gitsuper.github.home.model.Item;
import project.android.gitsuper.github.home.model.RepositoryModel;
import retrofit2.Call;
import retrofit2.Response;


@SuppressWarnings("ConstantConditions")
public class ItemDataSource extends PageKeyedDataSource<Integer, Item> {

    private ApiInterface apiInterface;

    public final MutableLiveData<DataLoadState> loadState;

    ItemDataSource(ApiInterface walmartApi) {
        this.apiInterface = walmartApi;
        loadState = new MutableLiveData<>();
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Item> callback) {

        loadState.postValue(DataLoadState.LOADING);
        Call<RepositoryModel> request = apiInterface.getRepositorysGithub(1);

        Response<RepositoryModel> response;
        try {
            response = request.execute();

            if (response != null) {
                callback.onResult(response.body().items, 1, 2);
            } else {
                callback.onResult(null, null, 2);
            }
            loadState.postValue(DataLoadState.LOADED);
        } catch (IOException ex) {
            loadState.postValue(DataLoadState.FAILED);
        }

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Item> callback) {

        loadState.postValue(DataLoadState.LOADING);

        Call<RepositoryModel> request = apiInterface.getRepositorysGithub(params.key);

        Response<RepositoryModel> response;
        try {
            response = request.execute();
            if (response != null) {
                Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                callback.onResult(response.body().items, adjacentKey);
            } else {
                callback.onResult(null, params.key - 1);
            }
            loadState.postValue(DataLoadState.LOADED);
        } catch (IOException ex) {
            loadState.postValue(DataLoadState.FAILED);
        }
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Item> callback) {

        loadState.postValue(DataLoadState.LOADING);

        Call<RepositoryModel> request = apiInterface.getRepositorysGithub(params.key);

        Response<RepositoryModel> response;
        try {
            response = request.execute();

            if (response != null) {
                if(response.body() != null){
                    if (response.body().items != null) {
                        if (response.body().items.size() > 0) {
                            callback.onResult(response.body().items, params.key + 1);
                        }
                    } else {
                        callback.onResult(null, params.key + 1);
                    }
                } else {
                    callback.onResult(null, params.key + 1);
                }
            } else {
                callback.onResult(null, params.key + 1);
            }
            loadState.postValue(DataLoadState.LOADED);
        } catch (IOException ex) {
            loadState.postValue(DataLoadState.FAILED);
        }
    }

}