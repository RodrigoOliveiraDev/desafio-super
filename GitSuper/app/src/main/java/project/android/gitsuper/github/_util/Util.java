package project.android.gitsuper.github._util;

import java.util.Random;

import project.android.gitsuper.R;

/**
 * Created by Rodrigo Oliveira on 26/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
public class Util {

    public static int returnRamdomAvatar() {
        return returnAvatar(ramdomNumber());
    }

    private static int returnAvatar(int i) {
        switch (i) {
            case 1:
                return R.drawable.avatar_01;
            case 2:
                return R.drawable.avatar_02;
            case 3:
                return R.drawable.avatar_03;
            case 4:
                return R.drawable.avatar_04;
            case 5:
                return R.drawable.avatar_05;
            case 6:
                return R.drawable.avatar_08;
            case 7:
                return R.drawable.avatar_07;
            case 8:
                return R.drawable.avatar_08;
            case 9:
                return R.drawable.avatar_09;
            case 10:
                return R.drawable.avatar_10;
            default:
                return R.drawable.ic_flat_user;
        }
    }

    private static int ramdomNumber() {
        int min = 1;
        int max = 10;
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

}
