package project.android.gitsuper._api.repository;


import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import project.android.gitsuper.github.home.model.DataLoadState;
import project.android.gitsuper.github.home.model.Item;

public interface ItemRepository {

    LiveData<PagedList<Item>> getRepositories();
    LiveData<DataLoadState> getDataLoadStatus();

}