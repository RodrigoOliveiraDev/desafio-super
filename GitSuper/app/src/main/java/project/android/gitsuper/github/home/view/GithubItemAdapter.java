package project.android.gitsuper.github.home.view;

import android.arch.paging.PagedListAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import project.android.gitsuper.R;
import project.android.gitsuper.databinding.RepoRowItemBinding;
import project.android.gitsuper.github.home.model.Item;

public class GithubItemAdapter extends PagedListAdapter<Item, GithubItemAdapter.ProductViewHolder> {

    private final OnActionListerner mActionListener;
    private LayoutInflater layoutInflater;

    GithubItemAdapter(OnActionListerner actionListerner) {
        super(DIFF_CALLBACK);
        this.mActionListener = actionListerner;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RepoRowItemBinding binding = DataBindingUtil.inflate(validInflater(parent), R.layout.repo_row_item, parent, false);
        return new ProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.binding.setPost(getItem(position));
        holder.binding.btViewDetails.setOnClickListener(view -> mActionListener.onDetailImport(getItem(position)));
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        RepoRowItemBinding binding;

        ProductViewHolder(RepoRowItemBinding view) {
            super(view.getRoot());
            this.binding = view;
        }
    }

    public interface OnActionListerner {
        void onDetailImport(Item item);
    }

    private LayoutInflater validInflater(ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        return layoutInflater;
    }

    private static final DiffUtil.ItemCallback<Item> DIFF_CALLBACK = new DiffUtil.ItemCallback<Item>() {
        @Override
        public boolean areItemsTheSame(@NonNull Item oldProduct, @NonNull Item newProduct) {
            return oldProduct.id.equals(newProduct.id);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Item oldProduct, @NonNull Item newProduct) {
            return oldProduct.equals(newProduct);
        }
    };

}