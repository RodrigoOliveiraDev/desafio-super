package project.android.gitsuper.github.home.model;



public enum DataLoadState {
        LOADING,
        LOADED,
        FAILED
}