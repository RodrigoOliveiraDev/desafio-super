package project.android.gitsuper.github.details.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import project.android.gitsuper.R;
import project.android.gitsuper.databinding.PullRowItemBinding;
import project.android.gitsuper.github.details.model.PullRequestModel;

/**
 * Created by Rodrigo Oliveira on 16/08/2018 for sac-digital-importacao.
 * ContactModel us rodrigooliveira.tecinfo@gmail.com
 */
public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsAdapter.RecyclerViewHolder> {

    private List<PullRequestModel> itemModelList;
    private LayoutInflater layoutInflater;
    private final OnActionListerner mActionListener;

    PullRequestsAdapter(List<PullRequestModel> borrowModelList, OnActionListerner actionListerner) {
        setHasStableIds(true);
        this.itemModelList = borrowModelList;
        this.mActionListener = actionListerner;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PullRowItemBinding binding = DataBindingUtil.inflate(validInflater(parent), R.layout.pull_row_item, parent, false);
        return new PullRequestsAdapter.RecyclerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, int position) {
        holder.binding.setPost(itemModelList.get(position));
        holder.itemView.setOnClickListener(view -> mActionListener.openPullRequestUrl(itemModelList.get(position).htmlUrl));
    }

    @Override
    public int getItemCount() {
        return itemModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return itemModelList.get(position).getId();
    }

    public void addItems(List<PullRequestModel> borrowModelList) {
        this.itemModelList = borrowModelList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private PullRowItemBinding binding;

        RecyclerViewHolder(PullRowItemBinding view) {
            super(view.getRoot());
            this.binding = view;
        }
    }

    private LayoutInflater validInflater(ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        return layoutInflater;
    }

    public interface OnActionListerner {
        void openPullRequestUrl(String url);
    }

}