package project.android.gitsuper._api;

import java.util.List;

import project.android.gitsuper.github.details.model.PullRequestModel;
import project.android.gitsuper.github.home.model.RepositoryModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Rodrigo Oliveira on 09/08/2018 for sac-digital-chat.
 * ContactModel us rodrigooliveira.tecinfo@gmail.com
 */
public interface ApiInterface {

    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoryModel> getRepositorysGithub(@Query("page") int page);

    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @GET("https://api.github.com/repos/{create}/{repository}/pulls")
    Call<List<PullRequestModel>> getPullRequests(@Path(value = "create", encoded = true) String create, @Path(value = "repository", encoded = true) String repository);

}