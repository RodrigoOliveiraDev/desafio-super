package project.android.gitsuper._api.data;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import project.android.gitsuper._api.ApiClient;
import project.android.gitsuper.github.home.model.Item;


public class ItemDataFactory extends DataSource.Factory<Integer, Item> {

    public MutableLiveData<ItemDataSource> datasourceLiveData = new MutableLiveData<>();

    @Override
    public DataSource<Integer, Item> create() {
        ItemDataSource dataSource = new ItemDataSource(ApiClient.get());
        datasourceLiveData.postValue(dataSource);
        return dataSource;
    }

}