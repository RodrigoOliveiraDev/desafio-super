package project.android.gitsuper._api.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.List;

import project.android.gitsuper.BuildConfig;
import project.android.gitsuper._api.ApiInterface;
import project.android.gitsuper.github.details.model.PullRequestModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rodrigo Oliveira on 28/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
public class PullRequestRepository {

    private ApiInterface apiInterface;
    private static PullRequestRepository projectRepository;

    private PullRequestRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    public synchronized static PullRequestRepository getInstance() {
        if (projectRepository == null) {
            projectRepository = new PullRequestRepository();
        }
        return projectRepository;
    }


    public LiveData<List<PullRequestModel>> getPullReqests(String create, String repository) {
        final MutableLiveData<List<PullRequestModel>> data = new MutableLiveData<>();

        apiInterface.getPullRequests(create, repository).enqueue(new Callback<List<PullRequestModel>>() {
            @Override
            public void onResponse(Call<List<PullRequestModel>> call, Response<List<PullRequestModel>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<PullRequestModel>> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;

    }

}
