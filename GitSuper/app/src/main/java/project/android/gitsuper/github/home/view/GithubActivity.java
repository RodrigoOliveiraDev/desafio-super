package project.android.gitsuper.github.home.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import project.android.gitsuper.R;
import project.android.gitsuper.databinding.ActivityGithubBinding;
import project.android.gitsuper.databinding.DrawerHeaderBinding;
import project.android.gitsuper.github._util.Util;
import project.android.gitsuper.github.details.view.GithubDetailActivity;
import project.android.gitsuper.github.home.model.Item;
import project.android.gitsuper.github.home.viewmodel.GithubViewModel;

public class GithubActivity extends AppCompatActivity implements GithubItemAdapter.OnActionListerner, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = GithubActivity.class.getSimpleName();

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private GithubViewModel viewModel;
    private GithubItemAdapter mAdapter;
    private ActivityGithubBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_github);

        configToolbar();
        configDrawer();
        configRecycleView();
        configViewModel();
        observeViewModel();
    }

    @Override
    public void onDetailImport(Item item) {
        Intent i = new Intent(GithubActivity.this, GithubDetailActivity.class);
        Bundle b = new Bundle();
        i.putExtra("item_selected", item);
        i.putExtras(b);
        startActivity(i);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_home) {
            closeDrawer();
        }
        binding.dlHome.closeDrawer(GravityCompat.START);
        return true;
    }

    private void configToolbar() {
        setSupportActionBar(binding.toolbarMain.toolbar);
        setTitle(R.string.app_name);

        binding.dlHome.addDrawerListener(actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                binding.dlHome,
                binding.toolbarMain.toolbar,
                R.string.app_name,
                R.string.app_name
        ));
        enableDrawer();
    }

    private void enableDrawer() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        binding.dlHome.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, GravityCompat.START);
        actionBarDrawerToggle.syncState();
        binding.navigationView.setNavigationItemSelectedListener(this);
    }

    private void closeDrawer() {
        if (binding.dlHome.isDrawerOpen(Gravity.START)) {
            binding.dlHome.closeDrawer(Gravity.START);
        } else if (binding.dlHome.isDrawerOpen(Gravity.END)) {
            binding.dlHome.closeDrawer(Gravity.END);
        }
    }

    private void configDrawer() {
        DrawerHeaderBinding headerBinding = DrawerHeaderBinding.bind(binding.navigationView.getHeaderView(0));
        headerBinding.tvNameDrawer.setText(getString(R.string.app_name));
        Glide.with(GithubActivity.this)
                .load(Util.returnRamdomAvatar())
                .apply(RequestOptions.circleCropTransform())
                .into(headerBinding.ivProfileDrawer);
    }

    private void configRecycleView() {
        mAdapter = new GithubItemAdapter(this);
        binding.rvMain.rvList.setAdapter(mAdapter);
        binding.rvMain.rvList.setHasFixedSize(true);
        binding.rvMain.rvList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void configViewModel() {
        viewModel = ViewModelProviders.of(this).get(GithubViewModel.class);
    }

    private void observeViewModel() {
        viewModel.dataLoadStatus().observe(this, loadStatus -> {
            assert loadStatus != null;
            switch (loadStatus) {
                case LOADING:
                    showLoadList();
                    break;
                case LOADED:
                    showRvImport();
                    break;
                case FAILED:
                    Toast.makeText(this, getString(R.string.s_end_list), Toast.LENGTH_LONG).show();
                    break;
            }
        });

        viewModel.getRepositories().observe(this, pagedList -> {
            if (pagedList.size() > 0) {
                mAdapter.submitList(pagedList);
            }
        });
    }

    private void showRvImport() {
        binding.loadMain.pbLoad.setVisibility(View.GONE);
        binding.rvMain.rvList.setVisibility(View.VISIBLE);
    }

    private void showLoadList() {
        binding.loadMain.pbLoad.setVisibility(View.VISIBLE);
    }

}