package project.android.gitsuper.github.details.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import project.android.gitsuper._api.repository.PullRequestRepository;
import project.android.gitsuper.github.details.model.PullRequestModel;

/**
 * Created by Rodrigo Oliveira on 26/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
public class GithubDetailViewModel extends AndroidViewModel {

    private LiveData<List<PullRequestModel>> projectListObservable;

    public GithubDetailViewModel(Application application) {
        super(application);
        projectListObservable = PullRequestRepository.getInstance().getPullReqests("", "");
    }

    /**
     * Expose the LiveData Projects query so the UI can observe it.
     */
    public LiveData<List<PullRequestModel>> getPullRequests(String c, String r) {
        projectListObservable = PullRequestRepository.getInstance().getPullReqests(c, r);
        return projectListObservable;
    }

}