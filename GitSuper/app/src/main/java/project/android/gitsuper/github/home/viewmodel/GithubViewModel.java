package project.android.gitsuper.github.home.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import project.android.gitsuper._api.repository.ItemRepository;
import project.android.gitsuper._api.repository.ItemRepositoryImpl;
import project.android.gitsuper.github.home.model.DataLoadState;
import project.android.gitsuper.github.home.model.Item;

/**
 * Created by Rodrigo Oliveira on 26/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
public class GithubViewModel extends AndroidViewModel {

    private ItemRepository repository;

    public GithubViewModel(@NonNull Application application) {
        super(application);
        repository = new ItemRepositoryImpl();
    }

    public LiveData<PagedList<Item>> getRepositories() {
        return repository.getRepositories();
    }

    public LiveData<DataLoadState> dataLoadStatus() {
        return repository.getDataLoadStatus();
    }

}