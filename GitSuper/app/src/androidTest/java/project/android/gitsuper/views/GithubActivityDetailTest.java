package project.android.gitsuper.views.views;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import project.android.gitsuper.R;
import project.android.gitsuper.github.details.view.GithubDetailActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

/**
 * Created by Rodrigo Oliveira on 28/08/2018 for GitSuper.
 * Contact us rodrigooliveira.tecinfo@gmail.com
 */
@RunWith(AndroidJUnit4.class)
public class GithubActivityDetailTest {

    @Rule
    public ActivityTestRule<GithubDetailActivity> mActivityRule = new ActivityTestRule<>(GithubDetailActivity.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState() {
        onView(ViewMatchers.withId(R.id.load_main)).check(matches(isDisplayed()));
        onView(withId(R.id.rv_main)).check(matches(not(isDisplayed())));
        onView(withId(R.id.tv_main)).check(matches(not(isDisplayed())));
    }


}
